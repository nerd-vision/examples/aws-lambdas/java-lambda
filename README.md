# Requirements

To use this example you will need to use java 1.8, or update the build.gradle to deploy to java 11.

## Configuration

You need to install and configure the [AWS cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) tools to deploy this example.

Once the AWS CLI is configured, you need to set the nerd.vision API key in the build.gradle.
 
## Build

1. Update the lambda environment with your nerd.vision API key
1. run `./gradlew --no-daemon shadowJar deploy`

## Invoke

To invoke the function you can use this command:

```bash
./gradlew --no-daemon invoke
```
