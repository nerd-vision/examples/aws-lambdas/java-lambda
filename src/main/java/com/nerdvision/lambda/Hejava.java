package com.nerdvision.lambda;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class Hejava {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

