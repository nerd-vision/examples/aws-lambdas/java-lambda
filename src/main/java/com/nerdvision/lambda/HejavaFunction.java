package com.nerdvision.lambda;

import com.nerdvision.api.NerdVision;
import io.micronaut.function.FunctionBean;
import io.micronaut.function.executor.FunctionInitializer;

import java.io.IOException;
import java.util.function.Function;

@FunctionBean("com/nerdvision/lambda")
public class HejavaFunction extends FunctionInitializer implements Function<Hejava, Hejava> {

    @Override
    public Hejava apply(Hejava msg) {
        NerdVision.lambda();

        final Hejava hejava = new Hejava();
        hejava.setName( msg.getName() );
        return hejava;
    }

    /**
     * This main method allows running the function as a CLI application using: echo '{}' | java -jar function.jar 
     * where the argument to echo is the JSON to be parsed.
     */
    public static void main(String...args) throws IOException {
        HejavaFunction function = new HejavaFunction();
        function.run(args, (context)-> function.apply(context.get(Hejava.class)));
    }    
}

