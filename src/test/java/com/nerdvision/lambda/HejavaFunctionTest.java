package com.nerdvision.lambda;

import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class HejavaFunctionTest {

    @Inject
    HejavaClient client;

    @Test
    public void testFunction() throws Exception {
    	Hejava body = new Hejava();
    	body.setName("hejava");
        assertEquals("hejava", client.apply(body).blockingGet().getName());
    }
}
