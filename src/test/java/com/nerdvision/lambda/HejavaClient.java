package com.nerdvision.lambda;

import io.micronaut.function.client.FunctionClient;
import io.micronaut.http.annotation.Body;
import io.reactivex.Single;
import javax.inject.Named;

@FunctionClient
public interface HejavaClient {

    @Named("hejava")
    Single<Hejava> apply(@Body Hejava body);

}
